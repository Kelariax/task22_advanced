const submit = document.getElementById("submit_button");
const tableContainer = document.getElementById("table_container");

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};
  
submit.addEventListener("click", () => {
    let table = document.querySelector(".table");
    if(table !== null)
    {
        table.remove();
    }
    table = document.createElement("table");
    table.className = "table";
    const rows = Number(document.getElementById("input_row").value);
    const columns = Number(document.getElementById("input_column").value);
    if(rows <= 0 || columns <= 0 || rows !== parseInt(rows) || columns !== parseInt(columns))
    {
        alert("Wrong input data!");
        return;
    }
    for(let i = 0; i < rows; i ++)
    {   
        let newTr = document.createElement("tr");
        for(let j = 0; j < columns; j++)
        {
            let newTd = document.createElement("td");
            newTd.innerHTML = `${i + 1}${j + 1}`;
            newTr.appendChild(newTd);
        }
        table.appendChild(newTr);
    }
    table.addEventListener("click", (e) => {
    if (e.offsetX > 1 && e.offsetY > 1 && e.target.matches("td"))
        e.target.style.backgroundColor = e.target.style.backgroundColor == "" ? getRandomColor() : "";
    });
    tableContainer.appendChild(table);
});

